import './style.scss';

import '@govflanders/vl-ui-core';
import '@govflanders/vl-ui-util';
// this `throws MultiSelect not defined`
import '@govflanders/vl-ui-multiselect';
// work-around so we dont block
// try {
//   require('@govflanders/vl-ui-multiselect');
// } catch (exc) {
//   console.error(exc);
// }
import '@govflanders/vl-ui-select';
import '@govflanders/vl-ui-pill-input';


function initialize() {
  console.log(window.vl);
}

document.addEventListener('DOMContentLoaded', initialize, false);