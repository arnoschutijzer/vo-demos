const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  webpack: {
    plugins: [
      new CopyWebpackPlugin([{
        from: 'node_modules/@govflanders/vl-ui-icon/dist/font',
        to: 'font'
      }])
    ]
  }
}