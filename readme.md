# vo demos

```bash
# install dependencies
$ npm install

# run example
$ npm run serve

# go to application
$ open http://localhost:9000
```